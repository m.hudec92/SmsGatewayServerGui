/**
 * Created by martinhudec on 24/04/2017.
 */


function callServerApiGet(call_name, params, callback) {
    $.get("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}

function callServerApiPost(call_name, params, callback) {
    $.post("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}


$(document).ready(function () {
    initDevicePage();
});

var initDevicePage = function () {

    callServerApiGet("getUserDevices", null, function (data) {
        console.log(JSON.stringify(data));

        var dataSet = [];
        for (var p in data.data) {
            if (data.data.hasOwnProperty(p)) {
                console.log(data.data[p].id);
                dataSet.push([
                    data.data[p].device_name,
                    data.data[p].description,
                    data.data[p].modified_at,
                    data.data[p].mobile_number,
                    data.data[p].shared_secret_key_negotiation_status,
                    data.data[p].device_id]
                );
            }
        }
        console.log(dataSet);
        console.log("devices table");
        var table = $('#devicesTable').DataTable({

            data: dataSet,
            columns: [
                {title: "Device name"},
                {title: "Description"},
                {title: "Modified at"},
                {title: "Mobile number"},
                {title: "Shared secret status"},
                {title: "Device Id"},
                {title: "Display detail"}

            ],
            "columnDefs": [{
                "targets": 6,
                "data": null,
                "defaultContent": "<button class='glyphicon glyphicon-eye-open' type='button' data-toggle='modal' data-target='#deviceDetail' ></button>"
            }, {
                "targets": 5,
                "visible": false,
                "searchable": false
            }]
        });

        $('#devicesTable').find('tbody').on('click', 'button', function () {
            var tableData = table.row($(this).parents('tr')).data();
            for (var p in data.data) {
                if (data.data.hasOwnProperty(p)) {
                    if (data.data[p].device_id === tableData[5]) {
                        for (var key in data.data[p]) {

                            if (data.data[p].hasOwnProperty(key))
                                $('input[name=' + key + ']').val(data.data[p][key]);

                        }
                        $('#api_key_panel_id').text(data.data[p].api_key);
                        if (data.data[p].has_custom_alphabet) {
                            $('#custom_alphabet').removeClass('hidden');
                            $('#custom_generated_alphabet_id').text(data.data[p].custom_alphabet);
                        } else {
                            $('#custom_alphabet').addClass('hidden');
                        }
                        if (data.data[p].shared_secret_key_negotiation_status) {
                            $('#device_actions').removeClass('hidden');
                            $('#device_actions_alert').removeClass('hidden');
                            $('#device_actions_error_alert').addClass('hidden');
                            $('#key_exchange_status').text(data.data[p].shared_secret_key_negotiation_status);
                            $('#key_exchange_status_modfied_at').text(data.data[p].modified_at);
                            if (data.data[p].shared_secret_key_negotiation_status != 'KEYS_GENERATED' && !data.data[p].shared_secret_key_negotiation_status.includes('ERROR')) {
                                $('#send_test_messgage_button_id').addClass('disabled');
                            }
                        } else {
                            $('#device_actions').addClass('hidden');
                            $('#device_actions_alert').addClass('hidden');
                            $('#device_actions_error_alert').removeClass('hidden');
                        }

                        if (data.data[p].ping_status_type) {
                            $('#test_message_status').text(data.data[p].ping_status_type);
                            $('#test_message_status_modfied_at').text(data.data[p].ping_status_type_modified_at);
                        }
                        // nacitame vsetky telefonne cisla na korych pocuva sms brana
                        callServerApiGet("getServerNumbers", null, function (data) {
                            console.log(JSON.stringify(data));
                            $('#serverGatewayNumbersPanelId').text(data.data[0].phone_numbers);
                        });
                    }
                }
            }
        });
    });
};

var reloadDevicePage = function () {

    callServerApiGet("getUserDevices", null, function (data) {
        console.log(JSON.stringify(data));

        var dataSet = [];
        for (var p in data.data) {
            if (data.data.hasOwnProperty(p)) {
                console.log(data.data[p].id);
                dataSet.push([
                    data.data[p].device_name,
                    data.data[p].description,
                    data.data[p].modified_at,
                    data.data[p].mobile_number,
                    data.data[p].shared_secret_key_negotiation_status,
                    data.data[p].device_id]
                );
            }
        }
        console.log(dataSet);
        console.log("devices table");

        $('#devicesTable').find('tbody').on('click', 'button', function () {
            var tableData = table.row($(this).parents('tr')).data();
            for (var p in data.data) {
                if (data.data.hasOwnProperty(p)) {
                    if (data.data[p].device_id === tableData[5]) {
                        for (var key in data.data[p]) {

                            if (data.data[p].hasOwnProperty(key))
                                $('input[name=' + key + ']').val(data.data[p][key]);

                        }
                        $('#api_key_panel_id').text(data.data[p].api_key);
                        if (data.data[p].has_custom_alphabet) {
                            $('#custom_alphabet').removeClass('hidden');
                            $('#custom_generated_alphabet_id').text(data.data[p].custom_alphabet);
                        } else {
                            $('#custom_alphabet').addClass('hidden');
                        }
                    }
                }
            }
        });
    });
};


callServerApiGet("getUserMobileNumbers", null, function (data) {
    $(document).ready(function () {
        $('#mobile_number_id').empty();
        $('#mobile_number_id').append($('<option>').text("Select number"));
        $.each(data.data, function (i, obj) {
            $('#mobile_number_id').append($('<option>').text(obj.mobile_number).attr('value', obj.number_id));
        });
        $('#device_detail_mobile_number_id').empty();
        $('#device_detail_mobile_number_id').append($('<option>').text("Select number"));
        $.each(data.data, function (i, obj) {
            console.log(obj);
            $('#device_detail_mobile_number_id').append($('<option>').text(obj.mobile_number).val(obj.number_id));
        });
    });
});

$("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
    $("#success-alert").slideUp(500);
});

$('#deviceDetailBasicId').submit(function (event) {
    event.preventDefault();
    var formData = $('#deviceDetailBasicId').serializeArray();
    console.log(formData);
    console.log($('#device_detail_mobile_number_id'));
    var mobile_number_id = $('#device_detail_mobile_number_id').val();
    console.log(mobile_number_id);
    var params = {
        formData: formData,
        'mobile_number_id': mobile_number_id
    };
    callServerApiPost('updateDeviceData', params, function (data) {
        console.log(data);
        if (data.status == 'success') {
            $('#device_detail_success_alert').removeClass('hidden');
            $('#device_detail_success_alert').delay(100).fadeIn();
            $('#device_detail_success_alert').delay(3000).fadeOut();
            $('#device_detail_success_alert').text('Device data was successfully updated');
        } else {
            $('#device_detail_error_alert').removeClass('hidden');
            $('#device_detail_error_alert').delay(100).fadeIn();
            $('#device_detail_error_alert').delay(3000).fadeOut();
            $('#device_detail_error_alert').text('Error while updating device data');
        }
    });
});

$('#data_set_upload_form_id').submit(function (event) {
    event.preventDefault();
    var deviceData = $('#deviceDetailBasicId').serializeArray();
    var formData = $('#data_set_upload_form_id').serializeArray();
    console.log(formData);
    var params = {
        formData: formData,
        deviceData: deviceData
    };
    callServerApiPost('generateCustomAlphabet', params, function (data) {
        console.log(data);
        if (data.status == 'success') {
            $('#device_detail_success_alert').removeClass('hidden');
            $('#device_detail_success_alert').delay(100).fadeIn();
            $('#device_detail_success_alert').delay(3000).fadeOut();
            $('#device_detail_success_alert').text('Custom alphabet was successfully generated');
        } else {
            $('#device_detail_error_alert').removeClass('hidden');
            $('#device_detail_error_alert').delay(100).fadeIn();
            $('#device_detail_error_alert').delay(3000).fadeOut();
            $('#device_detail_error_alert').text('Error during generating custom alphabet');
        }
    });
});

$('#send_test_messgage_button_id').click(function (event) {
    if ($('#send_test_messgage_button_id').hasClass('disabled')) {
        return;
    }
    $('#send_test_messgage_button_id').addClass('disabled');
    console.log(event);
    var deviceData = $('#deviceDetailBasicId').serializeArray();
    console.log(deviceData);
    var params = {
        deviceData: deviceData
    };

    callServerApiPost('sendTechnicalPingMessage', params, function (data) {
        console.log(data);
        if (data.status == 'success') {
            $('#device_detail_success_alert').removeClass('hidden');
            $('#device_detail_success_alert').delay(100).fadeIn();
            $('#device_detail_success_alert').delay(3000).fadeOut();
            $('#device_detail_success_alert').text('Test message was successfully sent');

            var myVar = setInterval(myTimer, 5000);

            function myTimer() {
                callServerApiGet('getTechnicalPingMessageStatus', params, function (data) {
                    $.each(data.data, function (i, obj) {
                        console.log(obj);
                        $('#test_message_status').text(obj.technical_message_status_type);
                        $('#test_message_status_modfied_at').text(obj.modified_at);
                        if (obj.technical_message_status_type == 'SERVER_CLIENT_TECHNICAL_ACK_RECEIVED') {
                            clearInterval(myVar);
                            $('#send_test_messgage_button_id').removeClass('disabled');
                        }
                    });
                });
            }


        } else {
            $('#device_detail_error_alert').removeClass('hidden');
            $('#device_detail_error_alert').delay(100).fadeIn();
            $('#device_detail_error_alert').delay(3000).fadeOut();
            $('#device_detail_error_alert').text('Error sending test message');
        }
    });
});

$('#start_key_exchange_button_id').click(function (event) {
    if ($('#start_key_exchange_button_id').hasClass('disabled')) {
        return;
    }
    $('#start_key_exchange_button_id').addClass('disabled');
    console.log(event);
    var deviceData = $('#deviceDetailBasicId').serializeArray();
    console.log(deviceData);
    var params = {
        deviceData: deviceData
    };

    callServerApiPost('sendKeyExchangeRequest', params, function (data) {
        console.log(data);
        if (data.status == 'success') {
            $('#device_detail_success_alert').removeClass('hidden');
            $('#device_detail_success_alert').delay(100).fadeIn();
            $('#device_detail_success_alert').delay(3000).fadeOut();
            $('#device_detail_success_alert').text('Key exchange request was successfully sent');

            var myVar = setInterval(myTimer, 5000);

            function myTimer() {
                callServerApiGet('getKeyExchangeStatus', params, function (data) {
                    $.each(data.data, function (i, obj) {
                        console.log(obj);
                        $('#key_exchange_status').text(obj.key_negotiation_status_type);
                        $('#key_exchange_status_modfied_at').text(obj.modified_at);

                    });
                });
            }


        } else {
            $('#device_detail_error_alert').removeClass('hidden');
            $('#device_detail_error_alert').delay(100).fadeIn();
            $('#device_detail_error_alert').delay(3000).fadeOut();
            $('#device_detail_error_alert').text('Error sending key exchange request');
        }
    });
});


var text_max = 1500;
$('#data_set_count_message').html(text_max + ' remaining');

$('#data_set').keyup(function () {
    var text_length = $('#data_set').val().length;
    var text_remaining = text_max - text_length;
    $('#data_set_count_message').html(text_remaining + ' remaining');
});