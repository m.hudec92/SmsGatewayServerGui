/**
 * Created by martinhudec on 27/04/2017.
 */
function callServerApiGet(call_name, params, callback) {
    $.get("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}

function callServerApiPost(call_name, params, callback) {
    $.post("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}


$(document).ready(function () {
    callServerApiGet('getServerWsdl', null, function (data) {
        $('#server_wsdl_id').text(data.data);
    });

    initDevicePage();
});

$("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
    $("#success-alert").slideUp(500);
});


var initDevicePage = function () {

    callServerApiGet("getUserWsSmsSettings", null, function (data) {
        console.log(JSON.stringify(data));

        var dataSet = [];
        for (var p in data.data) {
            if (data.data.hasOwnProperty(p)) {
                console.log(data.data[p].id);
                dataSet.push([
                    data.data[p].source_ip_address,
                    data.data[p].description,
                    data.data[p].modified_at,
                    data.data[p].created_at,
                    data.data[p].ws_auth_name,
                    data.data[p].ws_auth_password,
                    data.data[p].active_status,
                    data.data[p].setting_id]
                );
            }
        }
        console.log(dataSet);
        var table = $('#wsInForwardingtable').DataTable({

            data: dataSet,
            columns: [
                {title: "Source ip address"},
                {title: "Description"},
                {title: "Modified at"},
                {title: "Created at"},
                {title: "WS auth name"},
                {title: "WS auth password"},
                {title: "Activity status"},
                {title: "Setting id"},
                {title: "Display detail"}
            ],
            "columnDefs": [{
                "targets": 8,
                "data": null,
                "defaultContent": "<button class='glyphicon glyphicon-eye-open' type='button' data-toggle='modal' data-target='#ws_sms_setting_detail' ></button>"
            }, {
                "targets": 7,
                "visible": false,
                "searchable": false
            }]
        });

        $('#wsInForwardingtable').find('tbody').on('click', 'button', function () {
            var tableData = table.row($(this).parents('tr')).data();
            for (var p in data.data) {
                if (data.data.hasOwnProperty(p)) {
                    if (data.data[p].setting_id === tableData[7]) {
                        for (var key in data.data[p]) {
                            if (data.data[p].hasOwnProperty(key))
                                $('input[name=' + key + ']').val(data.data[p][key]);
                        }
                    }
                }
            }
        });
    });
};