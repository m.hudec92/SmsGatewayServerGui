/**
 * Created by martinhudec on 27/04/2017.
 */
function callServerApiGet(call_name, params, callback) {
    $.get("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}

function callServerApiPost(call_name, params, callback) {
    $.post("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}


$(document).ready(function () {

    initDevicePage();
});

$("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
    $("#success-alert").slideUp(500);
});


var initDevicePage = function () {

    callServerApiGet("getSentSMSmessages", null, function (data) {
        console.log(JSON.stringify(data));

        var dataSet = [];
        for (var p in data.data) {
            if (data.data.hasOwnProperty(p)) {
                console.log(data.data[p].id);
                dataSet.push([
                    data.data[p].destination_number,
                    data.data[p].device_name,
                    data.data[p].sent_at,
                    data.data[p].sms_sent_status_type,
                    data.data[p].message_uuid,
                    data.data[p].response_for_received_uuid,
                    data.data[p].response_for_email_uuid,
                    data.data[p].response_for_ws_uuid,
                    data.data[p].message_id]
                );
            }
        }
        console.log(dataSet);
        var table = $('#messageTable').DataTable({

            data: dataSet,
            columns: [
                {title: "Destination number"},
                {title: "Destination device name"},
                {title: "Sent at"},
                {title: "Sms sending status"},
                {title: "Message uuid"},
                {title: "Response for SMS uuid"},
                {title: "Response for email uuid"},
                {title: "Response for ws uuid"},
                {title: "Setting id"},
                {title: "Display detail"}
            ],
            "columnDefs": [{
                "targets": 9,
                "data": null,
                "defaultContent": "<button class='glyphicon glyphicon-eye-open' type='button' data-toggle='modal' data-target='#message_detail'></button>"
            }, {
                "targets": 8,
                "visible": false,
                "searchable": false
            }]
        });

        $('#messageTable').find('tbody').on('click', 'button', function () {

            var tableData = table.row($(this).parents('tr')).data();
            for (var p in data.data) {
                if (data.data.hasOwnProperty(p)) {
                    if (data.data[p].message_id === tableData[8]) {
                        callServerApiGet("getSentMessageDetail", {messageId: data.data[p].message_id}, function(data){
                            for (var key in data.body) {
                                console.log(data.body[key]);
                                if (data.body.hasOwnProperty(key))
                                    $('div[name=' + key + ']').text(data.body[key]);
                            }
                        });

                    }
                }
            }
        });
    });
};