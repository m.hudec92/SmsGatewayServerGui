/**
 * Created by martinhudec on 27/04/2017.
 */
function callServerApiGet(call_name, params, callback) {
    $.get("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}

function callServerApiPost(call_name, params, callback) {
    $.post("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}


$(document).ready(function () {
    callServerApiGet('getServerEmail', null, function (data) {
        $('#server_email_id').text(data.data);
    });
    initDevicePage();
});

$("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
    $("#success-alert").slideUp(500);
});


var initDevicePage = function () {

    callServerApiGet("getUserEmailSmsSettings", null, function (data) {
        console.log(JSON.stringify(data));

        var dataSet = [];
        for (var p in data.data) {
            if (data.data.hasOwnProperty(p)) {
                console.log(data.data[p].id);
                dataSet.push([
                    data.data[p].source_email_address,
                    data.data[p].description,
                    data.data[p].modified_at,
                    data.data[p].created_at,
                    data.data[p].active_status,
                    data.data[p].setting_id]
                );
            }
        }
        console.log(dataSet);
        var table = $('#forwardingTable').DataTable({

            data: dataSet,
            columns: [
                {title: "Source email address"},
                {title: "Description"},
                {title: "Modified at"},
                {title: "Created at"},
                {title: "Activity status"},
                {title: "Setting id"},
                {title: "Display detail"}
            ],
            "columnDefs": [{
                "targets": 6,
                "data": null,
                "defaultContent": "<button class='glyphicon glyphicon-eye-open' type='button' data-toggle='modal' data-target='#setting_detail' ></button>"
            }, {
                "targets": 5,
                "visible": false,
                "searchable": false
            }]
        });

        $('#forwardingTable').find('tbody').on('click', 'button', function () {
            var tableData = table.row($(this).parents('tr')).data();
            for (var p in data.data) {
                if (data.data.hasOwnProperty(p)) {
                    if (data.data[p].setting_id === tableData[5]) {
                        for (var key in data.data[p]) {
                            if (data.data[p].hasOwnProperty(key))
                                $('input[name=' + key + ']').val(data.data[p][key]);
                        }
                    }
                }
            }
        });
    });
};