/**
 * Created by martinhudec on 25/04/2017.
 */
function callServerApi(call_name, params, callback) {
    $.get("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}

callServerApi("getUserMobileNumbers", null, function (data) {
    console.log(JSON.stringify(data));

    var dataSet = [];
    for (var p in data.data) {
        if (data.data.hasOwnProperty(p)) {
            console.log(data.data[p].id);
            dataSet.push([
                data.data[p].mobile_number,
                data.data[p].operator,
                data.data[p].number_id]
            );
        }
    }
    console.log(dataSet);

    $(document).ready(function () {
        console.log("devices table");
        var table = $('#mobileNumbersTable').DataTable({
            data: dataSet,
            columns: [
                {title: "Mobile number"},
                {title: "Operator"},
                {title: "Mobile number Id"},
                {title: "Display detail"}

            ],
            "columnDefs": [{
                "targets": 3,
                "data": null,
                "defaultContent": "<button class='glyphicon glyphicon-eye-open' type='button' data-toggle='modal' data-target='#mobileNumberDetail'></button>"
            }, {
                "targets": 2,
                "visible": false,
                "searchable": false
            }]
        });

        $('#mobileNumbersTable').find('tbody').on('click', 'button', function () {
            var tableData = table.row($(this).parents('tr')).data();
            console.log("du");
            for (var p in data.data) {
                console.log(p);
                console.log(tableData);
                if (data.data.hasOwnProperty(p)) {
                    console.log(data.data[p].number_id);
                    if (data.data[p].number_id === tableData[2])
                        for (var key in data.data[p]) {
                            if (data.data[p].hasOwnProperty(key))
                                $('input[name=' + key + ']').val(data.data[p][key]);
                        }
                }
            }
        });
    });
});