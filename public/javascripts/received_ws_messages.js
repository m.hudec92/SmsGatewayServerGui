/**
 * Created by martinhudec on 27/04/2017.
 */
function callServerApiGet(call_name, params, callback) {
    $.get("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}

function callServerApiPost(call_name, params, callback) {
    $.post("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}


$(document).ready(function () {

    initDevicePage();
});

$("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
    $("#success-alert").slideUp(500);
});


var initDevicePage = function () {

    callServerApiGet("getReceivedWsmessages", null, function (data) {
        console.log(JSON.stringify(data));

        var dataSet = [];
        for (var p in data.data) {
            if (data.data.hasOwnProperty(p)) {

                dataSet.push([
                    data.data[p].source_ip_address,
                    data.data[p].received_at,
                    data.data[p].ws_message_uuid,
                    data.data[p].ws_received_status_type,
                    data.data[p].message_id]
                );
            }
        }

        var table = $('#messageTable').DataTable({

            data: dataSet,
            columns: [
                {title: "Source ip address"},
                {title: "Received at"},
                {title: "Message uuid"},
                {title: "Ws processed status"},
                {title: "Message id"},
                {title: "Display detail"}
            ],
            "columnDefs": [{
                "targets": 5,
                "data": null,
                "defaultContent": "<button class='glyphicon glyphicon-eye-open' type='button' data-toggle='modal' data-target='#message_detail'></button>"
            }, {
                "targets": 4,
                "visible": false,
                "searchable": false
            }]
        });

        $('#messageTable').find('tbody').on('click', 'button', function () {

            var tableData = table.row($(this).parents('tr')).data();
            for (var p in data.data) {
                if (data.data.hasOwnProperty(p)) {
                    if (data.data[p].message_id === tableData[4]) {
                        callServerApiGet("getReceivedWsMessageDetail", {messageId: data.data[p].message_id}, function(data){
                            console.log(data);
                            for (var key in data.data[0]) {
                                console.log(key);
                                if (data.data[0].hasOwnProperty(key))
                                    $('div[name=' + key + ']').text(data.data[0][key]);
                            }
                        });

                    }
                }
            }
        });
    });
};