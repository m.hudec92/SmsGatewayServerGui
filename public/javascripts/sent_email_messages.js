/**
 * Created by martinhudec on 27/04/2017.
 */
function callServerApiGet(call_name, params, callback) {
    $.get("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}

function callServerApiPost(call_name, params, callback) {
    $.post("api/" + call_name, params, function (data) {
        if (callback !== null) {
            callback(data);
        }
    }, 'json');
}


$(document).ready(function () {

    initDevicePage();
});

$("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
    $("#success-alert").slideUp(500);
});


var initDevicePage = function () {

    callServerApiGet("getSentEmailmessages", null, function (data) {
        console.log(JSON.stringify(data));

        var dataSet = [];
        for (var p in data.data) {
            if (data.data.hasOwnProperty(p)) {

                dataSet.push([
                    data.data[p].destination_mail_address,
                    data.data[p].sent_at,
                    data.data[p].response_for_received_sms,
                    data.data[p].message_id]
                );
            }
        }

        var table = $('#messageTable').DataTable({

            data: dataSet,
            columns: [
                {title: "Destination email address"},
                {title: "Received at"},
                {title: "Response for received sms uuid"},
                {title: "Message id"},
                {title: "Display detail"}
            ],
            "columnDefs": [{
                "targets": 4,
                "data": null,
                "defaultContent": "<button class='glyphicon glyphicon-eye-open' type='button' data-toggle='modal' data-target='#message_detail'></button>"
            }, {
                "targets": 3,
                "visible": false,
                "searchable": false
            }]
        });

        $('#messageTable').find('tbody').on('click', 'button', function () {

            var tableData = table.row($(this).parents('tr')).data();
            for (var p in data.data) {
                if (data.data.hasOwnProperty(p)) {
                    if (data.data[p].message_id === tableData[3]) {
                        callServerApiGet("getSentEmailMessageDetail", {messageId: data.data[p].message_id}, function(data){
                            console.log(data);
                            for (var key in data.data[0]) {
                                console.log(key);
                                if (data.data[0].hasOwnProperty(key))
                                    $('div[name=' + key + ']').text(data.data[0][key]);
                            }
                        });

                    }
                }
            }
        });
    });
};