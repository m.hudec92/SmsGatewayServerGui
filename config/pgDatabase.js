/**
 * Created by martinhudec on 24/04/2017.
 */
var promise = require('bluebird');
var configuration = require('./configuration.json');
var options = {
    promiseLib: promise
};

var pgp = require('pg-promise')(options);

var connection = {
    host: 'localhost',
    port: 5432,
    database: 'smsgateway',
    user: configuration.PostgreSqlConfiguration.UserName,
    password: configuration.PostgreSqlConfiguration.Password
};



module.exports= {
    db_instance: pgp(connection)
};

