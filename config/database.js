/**
 * Created by martinhudec on 21/04/2017.
 */
var configuration = require('./configuration.json');

module.exports = {

    'url' : 'mongodb://'+configuration.MongoDbConfiguration.UserName+':'+configuration.MongoDbConfiguration.Password+'@localhost:27017/passport'

};
