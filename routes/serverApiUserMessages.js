/**
 * Created by martinhudec on 24/04/2017.
 */

var db_connector = require('../config/pgDatabase.js');
var smsGatewayBI = require('../bussinessLogic/smsGatewayBI.js');
console.log("getting data");


function getReceivedSMSmessages(req, res, next) {
    console.log("Getting all received sms for user " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select sms.id as message_id, 
    sms.message_uuid, 
    sms.sender_number, 
    sms.sms_received_status_type,
    cscd.device_name,
    sms."timestamp" as received_at
    from sms_received as sms
    left join client_secure_communication_data cscd on sms.client_id = cscd.id 
    left join registered_user ruser on cscd.user_id = ruser.id
    where ruser.email = '` + req.user.local.email + "'" +
        "order by sms.\"timestamp\" desc")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getMessageDetail(req, res) {
    console.log("getting message detail " + req.user);


    smsGatewayBI.getMessageDetail(req.query.messageId, function (error, result, body) {
        console.log("status code " + result.statusCode);
        if (result.statusCode == 200) {
            res.status(200).json({
                status: 'success',
                body: body
            });
        } else {
            res.status(200).json({
                status: 'error'
            });
        }
    });
}

function getSentSMSmessages(req, res, next) {
    console.log("Getting all received sms for user " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select sms.id as message_id, 
    sms.message_uuid, 
    sms.destination_number, 
    sms.sms_sent_status_type,
    cscd.device_name,
    sms."timestamp" as sent_at,
    sms_rec.message_uuid as response_for_received_uuid,
    email_rec.email_id as response_for_email_uuid,
    ws_rec.ws_message_uuid as response_for_ws_uuid
    from sms_sent as sms
    left join client_secure_communication_data cscd on sms.client_id = cscd.id 
    left join sms_received sms_rec on sms.response_for_received_sms_id = sms_rec.id
    left join email_received email_rec on sms.response_for_received_email_id = email_rec.id
    left join ws_received ws_rec on sms.response_for_received_ws_id = ws_rec.id
    left join registered_user ruser on cscd.user_id = ruser.id
    where ruser.email = '` + req.user.local.email + "'" +
        "order by sms.\"timestamp\" desc")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getSentMessageDetail(req, res) {
    console.log("getting message detail " + req.user);


    smsGatewayBI.getSentMessageDetail(req.query.messageId, function (error, result, body) {
        console.log("status code " + result.statusCode);
        if (result.statusCode == 200) {
            res.status(200).json({
                status: 'success',
                body: body
            });
        } else {
            res.status(200).json({
                status: 'error'
            });
        }
    });
}


// EMAIL RECEIVED

function getReceivedEmailmessages(req, res, next) {
    console.log("Getting all received emails for user " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select e.id as message_id, 
    e.email_id, 
    e.source_mail_address, 
    e.email_received_status_type,
   	e.created_at as received_at
    from email_received as e
    left join email_forward_in_settings fw_in on e.email_forward_in_id = fw_in.id
    left join registered_user ruser on fw_in.user_id = ruser.id
    where ruser.email = '` + req.user.local.email + "'" +
        "order by e.\"created_at\" desc")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getReceivedEmailMessageDetail(req, res) {
    console.log("getting message detail " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select e.source_mail_address, e.subject, e."data" from email_received e where e.id  = '` + req.query.messageId + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

// EMAIL SENT

function getSentEmailmessages(req, res, next) {
    console.log("Getting all received emails for user " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select e.id as message_id,  
    e.destination_mail_address, 
   	e.created_at as sent_at,
   	sms_rec.message_uuid as response_for_received_sms
    from email_sent as e
    left join email_forward_out_settings fw_out on e.email_forward_out_id = fw_out.id
    left join client_secure_communication_data cscd on cscd.id = fw_out.client_id
    left join registered_user ruser on cscd.user_id = ruser.id
    left join sms_received sms_rec on sms_rec.id = e.sms_received_id
    where ruser.email = '` + req.user.local.email + "'" +
        "order by e.\"created_at\" asc")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getSentEmailMessageDetail(req, res) {
    console.log("getting message detail " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select e.destination_mail_address, e.subject, e."data" from email_sent e where e.id  = '` + req.query.messageId + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

// WS RECEIVED

function getReceivedWsmessages(req, res, next) {
    console.log("Getting all received emails for user " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select ws.id as message_id,  
    ws.source_ip_address, 
   	ws.created_at as received_at,
   	ws.destination,
   	ws.ws_received_status_type,
   	ws.ws_message_uuid
    from ws_received as ws
    left join ws_forward_in_settings fw_in on ws.ws_forward_in_id = fw_in.id 
    left join registered_user ruser on fw_in.user_id = ruser.id
    where ruser.email = '` + req.user.local.email + "'" +
        "order by ws.\"created_at\" desc")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getReceivedWsMessageDetail(req, res) {
    console.log("getting message detail " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select ws.source_ip_address, ws.destination, ws."data" from ws_received ws where ws.id  = '` + req.query.messageId + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}


// WS SENT

function getSentWsmessages(req, res, next) {
    console.log("Getting all received emails for user " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select 
    ws.id as message_id, 
    ws.created_at as sent_at, 
    ws.ws_message_uuid, 
    ws.ws_sent_status_type, 
    sms_rec.message_uuid as received_message_response_uuid
    from ws_sent ws
   left join ws_forward_out_settings ws_out on ws.ws_forward_out_id = ws_out.id
   left join sms_received sms_rec on ws.sms_received_id = sms_rec.id
   left join client_secure_communication_data cscd on ws_out.client_id = cscd.id
   left join registered_user ruser on ruser.id = cscd.user_id
    where ruser.email = '` + req.user.local.email + "'" +
        "order by ws.\"created_at\" asc")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getSentWsMessageDetail(req, res) {
    console.log("getting message detail " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select ws.data, ws_out.destination_ws_wsdl_url, ws_out.ws_port_name, ws_out.ws_service_name  from ws_sent ws
    left join ws_forward_out_settings ws_out on ws_out.id = ws.ws_forward_out_id where ws.id = '` + req.query.messageId + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

module.exports = {
    getReceivedSMSmessages: getReceivedSMSmessages,
    getMessageDetail: getMessageDetail,
    getSentSMSmessages: getSentSMSmessages,
    getSentMessageDetail: getSentMessageDetail,
    getReceivedEmailmessages: getReceivedEmailmessages,
    getReceivedEmailMessageDetail: getReceivedEmailMessageDetail,
    getSentEmailmessages: getSentEmailmessages,
    getSentEmailMessageDetail: getSentEmailMessageDetail,
    getReceivedWsmessages: getReceivedWsmessages,
    getReceivedWsMessageDetail: getReceivedWsMessageDetail,
    getSentWsmessages:getSentWsmessages,
    getSentWsMessageDetail:getSentWsMessageDetail
};