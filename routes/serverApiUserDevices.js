/**
 * Created by martinhudec on 24/04/2017.
 */

var db_connector = require('../config/pgDatabase.js');
var smsGatewayBI = require('../bussinessLogic/smsGatewayBI.js');
console.log("getting data");
function getUserDevices(req, res, next) {
    console.log("Getting all getUserDevices for user " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select 
        cscd.id as device_id,
        cscd.created_at,
        cscd.modified_at,
        cscd.description, 
        cscd.device_name,
        cscd.api_key,
        cscd.aes_key_modified_at,
        cscd.api_key_modified_at,
        mnumber.mobile_number,
        (select st.technical_message_status_type from sms_technical as st
    		where st.mobile_number_id = mnumber.id and st.technical_message_type = 'PING' order by st.created_at desc limit 1) as ping_status_type,
    		(select st.modified_at from sms_technical as st
    		where st.mobile_number_id = mnumber.id and st.technical_message_type = 'PING' order by st.created_at desc limit 1) as ping_status_type_modified_at,
        case when cscd.custom_alphabet is not null
            then true
        else false
    end as has_custom_alphabet,
    cscd.custom_alphabet,
    cscd.key_negotiation_status_type as shared_secret_key_negotiation_status,
        cscd.new_api_key_modified_at,
    cscd.key_renegotiation_status_type as api_key_negotiation_status
    from  client_secure_communication_data cscd 
   	left join registered_user ruser on cscd.user_id = ruser.id
    left join mobile_numbers mnumber on mnumber.id = cscd.mobile_number_id
    where ruser.email = '` + req.user.local.email + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function registerNewDevice(req, res, settings) {
    console.log("registerNewDevice for user " + req.user);

    console.log(req.body);
    smsGatewayBI.registerNewDevice(req.user.local.email, req.body.device_name, req.body.description, req.body.number_id, function (error, result, statusCode) {
        console.log("status code " + statusCode);
        if (statusCode !== 200) {
            console.error(error);
            console.log(result);
            req.flash('errorMessage', 'error');
            res.redirect(settings.redirectTo);

        } else {
            req.flash('successMessage', 'Data was successfully updated');
            res.redirect(settings.redirectTo);
        }
    });

}

function getUserMobileNumbers(req, res, next) {
    console.log("Getting all user mobile numbers for user " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select 
    mnumber.mobile_number, 
    mnumber.id as number_id,
    operator.value as operator
    from mobile_numbers mnumber 
    left join  registered_user ruser on mnumber.user_id = ruser.id
    left join class_operator_type operator on operator.id = mnumber.operator_id
    where ruser.email = '` + req.user.local.email + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getTechnicalPingMessageStatus(req, res, next) {
    console.log("Getting last technical message status for user " + req.user);
    console.log(req.query);
    var values = {};
    req.query.deviceData.forEach(function (value) {
        values[value.name] = value.value;
    });
    console.log(values);
    //noinspection JSAnnotator
    db_connector.db_instance.any(`select st.technical_message_status_type, st.modified_at from sms_technical as st
    left join client_secure_communication_data cscd on cscd.mobile_number_id = st.mobile_number_id
    where cscd.id = ` + values.device_id + ` and st.technical_message_type = 'PING' order by st.created_at desc limit 1`)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getKeyExchangeStatus(req, res, next) {
    console.log("Getting last key exchange message status for user " + req.user);
    console.log(req.query);
    var values = {};
    req.query.deviceData.forEach(function (value) {
        values[value.name] = value.value;
    });
    console.log(values);
    //noinspection JSAnnotator
    db_connector.db_instance.any(`select key_negotiation_status_type, modified_at from client_secure_communication_data where id = ` + values.device_id)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function updateUserMobileNumberData(req, res, settings) {
    console.log("updating mobile number data for user " + req.user);

    console.log(req.body);
    var operator_id;
    switch (req.body.operator) {
        case ('ORANGE'):
            operator_id = 1;
            break;
        case ('TELEKOM'):
            operator_id = 2;
            break;
        case ('O2'):
            operator_id = 3;
            break;
        case ('4KA'):
            operator_id = 4;
            break;

    }
    if (req.body.number_id) {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`
    update mobile_numbers set modified_at = now(), mobile_number = '` + req.body.mobile_number + `',
    operator_id = '` + operator_id + `'
    where id = '` + req.body.number_id + `'`)
            .then(function (data) {
                console.log(data);
                req.flash('successMessage', 'Data was successfully updated');
                res.redirect(settings.redirectTo);
            })

            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'error');
                res.redirect(settings.redirectTo);
            });
    } else {
        console.log("creating mobile number data for user " + req.user);
        //noinspection JSAnnotator
        db_connector.db_instance.any(`
    insert into mobile_numbers (created_at, modified_at, mobile_number, operator_id, user_id) values (now(),now(),'` + req.body.mobile_number + `',
    '` + operator_id + `', (select 
        ruser.id
        from registered_user ruser
        where ruser.email = '` + req.user.local.email + `'))`)
            .then(function (data) {
                console.log(data);
                req.flash('successMessage', 'Data was successfully saved');
                res.redirect(settings.redirectTo);
            })

            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'error');
                res.redirect(settings.redirectTo);
            });
    }
}


function updateDeviceData(req, res) {
    console.log(req.body);
    console.log("updating device data for user " + req.user.local.email);
    var values = {};
    req.body.formData.forEach(function (value) {
        values[value.name] = value.value;
    });
    console.log(values);
    //noinspection JSAnnotator
    db_connector.db_instance.any(`
    update client_secure_communication_data set modified_at = now(), mobile_number_id = '` + req.body.mobile_number_id + `',
    device_name='` + values.device_name + `', description= '` + values.description + `'
    where id = '` + values.device_id + `'`)
        .then(function (data) {

            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            res.status(200).json({
                status: 'error',
                data: undefined,
                message: err.message
            });
        });
}

function generateCustomAlphabet(req, res) {
    console.log("generating new alphabet for user " + req.user);
    var values = {};
    req.body.formData.forEach(function (value) {
        values[value.name] = value.value;
    });
    req.body.deviceData.forEach(function (value) {
        values[value.name] = value.value;
    });

    console.log(values);
    smsGatewayBI.generateCustomAlphabet(req.user.local.email, values.data_set, values.device_id, function (error, result, statusCode) {
        console.log("status code " + statusCode);
        if (statusCode == 200) {
            res.status(200).json({
                status: 'success'
            });
        } else {
            res.status(200).json({
                status: 'error'
            });
        }
    });
}

function sendTechnicalPingMessage(req, res) {
    console.log("sending test message for user " + req.user);
    var values = {};
    req.body.deviceData.forEach(function (value) {
        values[value.name] = value.value;
    });

    console.log(values);
    smsGatewayBI.sendTechnicalPingMessage(req.user.local.email, values.device_id, function (error, result, statusCode) {
        console.log("status code " + statusCode);
        if (statusCode == 200) {
            res.status(200).json({
                status: 'success'
            });
        } else {
            res.status(200).json({
                status: 'error'
            });
        }
    });
}

function sendKeyExchangeRequest(req, res) {
    console.log("sending key exchange request message for user " + req.user);
    var values = {};
    req.body.deviceData.forEach(function (value) {
        values[value.name] = value.value;
    });

    console.log(values);
    smsGatewayBI.sendKeyExchangeRequest(req.user.local.email, values.device_id, function (error, result, statusCode) {
        console.log("status code " + statusCode);
        if (statusCode == 200) {
            res.status(200).json({
                status: 'success'
            });
        } else {
            res.status(200).json({
                status: 'error'
            });
        }
    });
}


function getServerNumbers(req, res) {
    console.log("getting server numbers " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select string_agg(phone_number, ', ')as phone_numbers from gateway_modems where status = 'CONNECTED'`)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}


module.exports = {
    getUserDevices: getUserDevices,
    getServerNumbers: getServerNumbers,
    registerNewDevice: registerNewDevice,
    getUserMobileNumbers: getUserMobileNumbers,
    updateUserMobileNumberData: updateUserMobileNumberData,
    updateDeviceData: updateDeviceData,
    generateCustomAlphabet: generateCustomAlphabet,
    sendTechnicalPingMessage: sendTechnicalPingMessage,
    getTechnicalPingMessageStatus: getTechnicalPingMessageStatus,
    sendKeyExchangeRequest: sendKeyExchangeRequest,
    getKeyExchangeStatus: getKeyExchangeStatus,
};