/**
 * Created by martinhudec on 10/05/2017.
 */


function getMessagesCountStats(req, res, next) {
    console.log("Getting messagesCountStats " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select 
    mnumber.mobile_number, 
    mnumber.id as number_id,
    operator.value as operator
    from mobile_numbers mnumber 
    left join  registered_user ruser on mnumber.user_id = ruser.id
    left join class_operator_type operator on operator.id = mnumber.operator_id
    where ruser.email = '` + req.user.local.email + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: {
                        labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
                        datasets: [{
                            label: 'apples',
                            data: [12, 19, 3, 17, 6, 3, 7],
                            backgroundColor: "rgba(153,255,51,0.4)"
                        }, {
                            label: 'oranges',
                            data: [2, 29, 5, 5, 2, 3, 10],
                            backgroundColor: "rgba(255,153,0,0.4)"
                        }]
                    },
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

module.exports = {
    getMessagesCountStats: getMessagesCountStats

};