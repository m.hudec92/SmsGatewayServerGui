var express = require('express');
var passport = require('passport');
var router = express.Router();
var userDevicesServerApi = require('./serverApiUserDevices.js');
var userForwardingSettingsServerApi = require('./serverApiUserForwardingSettings.js');
var userMessagesServerApi = require('./serverApiUserMessages.js');
var stats = require('./stats.js');
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

router.get('/login', function (req, res, next) {
    res.render('login.ejs', {message: req.flash('loginMessage')});
});

router.get('/signup', function (req, res) {
    res.render('signup.ejs', {message: req.flash('signupMessage')});
});

router.get('/profile', isLoggedIn, function (req, res) {
    res.render('profile.ejs', {user: req.user});
});

router.get('/devices', isLoggedIn, function (req, res) {

    res.render('devices.ejs', {user: req.user, success: req.flash('successMessage'), error: req.flash('errorMessage')});

});

router.get('/mobile_numbers', isLoggedIn, function (req, res) {

    res.render('mobile_numbers.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

router.get('/ws_sms_forwarding', isLoggedIn, function (req, res) {

    res.render('ws_sms_forwarding.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

router.get('/sms_ws_forwarding', isLoggedIn, function (req, res) {

    res.render('sms_ws_forwarding.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});
router.get('/email_sms_forwarding', isLoggedIn, function (req, res) {

    res.render('email_sms_forwarding.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});
router.get('/sms_email_forwarding', isLoggedIn, function (req, res) {

    res.render('sms_email_forwarding.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

router.get('/received_sms', isLoggedIn, function (req, res) {
    res.render('received_sms_messages.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

router.get('/sent_sms', isLoggedIn, function (req, res) {
    res.render('sent_sms_messages.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

router.get('/received_email', isLoggedIn, function (req, res) {
    res.render('received_email_messages.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

router.get('/sent_email', isLoggedIn, function (req, res) {
    res.render('sent_email_messages.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

router.get('/received_ws', isLoggedIn, function (req, res) {
    res.render('received_ws_messages.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

router.get('/sent_ws', isLoggedIn, function (req, res) {
    res.render('sent_ws_messages.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });

});

/*
router.get('/stats', isLoggedIn, function (req, res) {
    res.render('stats.ejs', {
        user: req.user,
        success: req.flash('successMessage'),
        error: req.flash('errorMessage')
    });
});*/


router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});


router.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/profile',
    failureRedirect: '/signup',
    failureFlash: true
}));

router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/profile',
    failureRedirect: '/login',
    failureFlash: true
}));


// DEVICES TAB
router.post('/devices/registerNewDevice', isLoggedIn, function (req, res) {
    userDevicesServerApi.registerNewDevice(req, res, {redirectTo: '/devices'})
});

router.post('/numbers/updateMobileNumberData', isLoggedIn, function (req, res) {
    userDevicesServerApi.updateUserMobileNumberData(req, res, {redirectTo: '/mobile_numbers'})
});





router.get('/api/getUserDevices', isLoggedIn, userDevicesServerApi.getUserDevices);
router.get('/api/getServerNumbers', isLoggedIn, userDevicesServerApi.getServerNumbers);
router.get('/api/getUserMobileNumbers', isLoggedIn, userDevicesServerApi.getUserMobileNumbers);
router.get('/api/getTechnicalPingMessageStatus', isLoggedIn, userDevicesServerApi.getTechnicalPingMessageStatus);
router.get('/api/getKeyExchangeStatus', isLoggedIn, userDevicesServerApi.getKeyExchangeStatus);
router.post('/api/updateDeviceData', isLoggedIn, userDevicesServerApi.updateDeviceData);
router.post('/api/generateCustomAlphabet', isLoggedIn, userDevicesServerApi.generateCustomAlphabet);
router.post('/api/sendTechnicalPingMessage', isLoggedIn, userDevicesServerApi.sendTechnicalPingMessage);
router.post('/api/sendKeyExchangeRequest', isLoggedIn, userDevicesServerApi.sendKeyExchangeRequest);


// WS-> SMS SETTINGS

router.get('/api/getUserWsSmsSettings', isLoggedIn, userForwardingSettingsServerApi.getUserWsSmsSettings);
router.get('/api/getServerWsdl', isLoggedIn, userForwardingSettingsServerApi.getServerWsdl);
router.post('/api/updateWsSmsSettings', isLoggedIn, function (req, res) {
    userForwardingSettingsServerApi.updateWsSmsSetting(req, res, {redirectTo: '/ws_sms_forwarding'})
});

// SMS -> WS SETTINGS

router.get('/api/getUserSmsWsSettings', isLoggedIn, userForwardingSettingsServerApi.getUserSmsWsSettings);
router.post('/api/updateSmsWsSettings', isLoggedIn, function (req, res) {
    userForwardingSettingsServerApi.updateSmsWsSetting(req, res, {redirectTo: '/sms_ws_forwarding'})
});

// EMAIL -> SMS SETTINGS

router.get('/api/getUserEmailSmsSettings', isLoggedIn, userForwardingSettingsServerApi.getUserEmailSmsSettings);
router.get('/api/getServerEmail', isLoggedIn, userForwardingSettingsServerApi.getServerEmail);
router.post('/api/updateEmailSmsSettings', isLoggedIn, function (req, res) {
    userForwardingSettingsServerApi.updateEmailSmsSettings(req, res, {redirectTo: '/email_sms_forwarding'})
});

// SMS -> EMAIL SETTINGS

router.get('/api/getUserSmsEmailSettings', isLoggedIn, userForwardingSettingsServerApi.getUserSmsEmailSettings);
router.post('/api/updateSmsEmailSettings', isLoggedIn, function (req, res) {
    userForwardingSettingsServerApi.updateSmsEmailSetting(req, res, {redirectTo: '/sms_email_forwarding'})
});

// SMS RECEIVED

router.get('/api/getReceivedSMSmessages', isLoggedIn, userMessagesServerApi.getReceivedSMSmessages);
router.get('/api/getMessageDetail', isLoggedIn, userMessagesServerApi.getMessageDetail);

// SMS SENT

router.get('/api/getSentSMSmessages', isLoggedIn, userMessagesServerApi.getSentSMSmessages);
router.get('/api/getSentMessageDetail', isLoggedIn, userMessagesServerApi.getSentMessageDetail);

// EMAIL RECEIVED

router.get('/api/getReceivedEmailmessages', isLoggedIn, userMessagesServerApi.getReceivedEmailmessages);
router.get('/api/getReceivedEmailMessageDetail', isLoggedIn, userMessagesServerApi.getReceivedEmailMessageDetail);

// EMAIL SENT

router.get('/api/getSentEmailMessages', isLoggedIn, userMessagesServerApi.getSentEmailmessages);
router.get('/api/getSentEmailMessageDetail', isLoggedIn, userMessagesServerApi.getSentEmailMessageDetail);

// WS RECEIVED

router.get('/api/getReceivedWsmessages', isLoggedIn, userMessagesServerApi.getReceivedWsmessages);
router.get('/api/getReceivedWsMessageDetail', isLoggedIn, userMessagesServerApi.getReceivedWsMessageDetail);

// WS SENT

router.get('/api/getSentWsmessages', isLoggedIn, userMessagesServerApi.getSentWsmessages);
router.get('/api/getSentWsMessageDetail', isLoggedIn, userMessagesServerApi.getSentWsMessageDetail);

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

module.exports = router;
