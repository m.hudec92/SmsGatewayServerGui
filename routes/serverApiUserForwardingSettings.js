/**
 * Created by martinhudec on 27/04/2017.
 */
var db_connector = require('../config/pgDatabase.js');
var smsGatewayBI = require('../bussinessLogic/smsGatewayBI.js');
const uuidV4 = require('uuid/v4');

// WS ->  SMS
function getUserWsSmsSettings(req, res, next) {
    console.log("Getting all ws in settings " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select ws.id as setting_id, 
    ws.description, ws.source_ip_address, 
    ws.created_at, ws.modified_at, 
    ws.ws_auth_name, 
    ws.ws_auth_password,
    ws.active_status 
     from ws_forward_in_settings as ws
left join registered_user ruser on ws.user_id = ruser.id where ruser.email = '` + req.user.local.email + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getServerWsdl(req, res) {
    console.log("getting server wsdl " + req.user);

    smsGatewayBI.getServerWsdl(req.user.local.email, function (error, result, body) {
        if (result.statusCode == 200) {
            res.status(200).json({
                status: 'success',
                data: body
            });
        } else {
            res.status(200).json({
                status: 'error'
            });
        }
    });
}

function updateWsSmsSetting(req, res, settings) {

    console.log("updating ws -> sms mapping for user" + req.user.local.email);

    console.log(req.body);

    if (req.body.setting_id) {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`update ws_forward_in_settings set 
        modified_at = now(), 
        source_ip_address = '` + req.body.source_ip_address + `', 
        description = '` + req.body.description + `', 
        ws_auth_name = '` + req.body.ws_auth_name + `', 
        ws_auth_password = '` + req.body.ws_auth_password + `', 
        active_status = '` + req.body.active_status + `'
            where id = '` + req.body.setting_id + `'`)
            .then(function (data) {
                req.flash('successMessage', 'Data was successfully updated');
                res.redirect(settings.redirectTo);
            })
            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'error');
                res.redirect(settings.redirectTo);
            });
    } else {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`insert into ws_forward_in_settings  (modified_at, created_at, source_ip_address, description, ws_auth_name, ws_auth_password, active_status, user_id) values (now(), now(), '` + req.body.source_ip_address + `','` + req.body.description +
            `','` + req.body.ws_auth_name +
            `','` + req.body.ws_auth_password +
            `','` + req.body.active_status +
            `',(select id from registered_user where email = '` + req.user.local.email + `'))`
        )
            .then(function (data) {
                req.flash('successMessage', 'New ws -> sms mapping was successfully created');
                res.redirect(settings.redirectTo);
            })
            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'Error while creating new ws -> sms mapping');
                res.redirect(settings.redirectTo);
            });
    }
};

// SMS -> WS
function getUserSmsWsSettings(req, res, next) {
    console.log("Getting all ws out settings " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select ws.destination_ws_wsdl_url,
    ws.ws_port_name,
    ws.ws_namespace_uri,
    ws.ws_service_name,
    ws.created_at,
    ws.modified_at,
    ws.description,
    ws.active_status,
    ws.client_id,
    ws.id as setting_id,
    cscd.device_name,
cscd.id as device_id
from ws_forward_out_settings as ws
left join client_secure_communication_data cscd on ws.client_id = cscd.id
left join registered_user ruser on cscd.user_id = ruser.id where ruser.email = '` + req.user.local.email + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}


function updateSmsWsSetting(req, res, settings) {

    console.log("updating sms -> ws mapping for user" + req.user.local.email);

    console.log(req.body);

    if (req.body.setting_id) {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`update ws_forward_out_settings set 
        modified_at = now(), 
        destination_ws_wsdl_url = '` + req.body.destination_ws_wsdl_url + `', 
        description = '` + req.body.description + `', 
        ws_service_name = '` + req.body.ws_service_name + `', 
        ws_namespace_uri = '` + req.body.ws_namespace_uri + `', 
        ws_port_name = '` + req.body.ws_port_name + `', 
        client_id = '` + req.body.device_id + `',
        active_status = '` + req.body.active_status + `'
            where id = '` + req.body.setting_id + `'`)
            .then(function (data) {
                req.flash('successMessage', 'Data was successfully updated');
                res.redirect(settings.redirectTo);
            })
            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'error');
                res.redirect(settings.redirectTo);
            });
    } else {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`insert into ws_forward_out_settings  (
        modified_at, 
        created_at, 
        destination_ws_wsdl_url, 
        description, 
        ws_port_name, 
        ws_namespace_uri, 
        ws_service_name, 
        active_status, 
        client_id) values 
        (now(), 
        now(), 
            '` + req.body.destination_ws_wsdl_url +
            `','` + req.body.description +
            `','` + req.body.ws_port_name +
            `','` + req.body.ws_namespace_uri +
            `','` + req.body.ws_service_name +
            `','` + req.body.active_status +
            `','` + req.body.device_id +
            `')`
        )
            .then(function (data) {
                req.flash('successMessage', 'New ws -> sms mapping was successfully created');
                res.redirect(settings.redirectTo);
            })
            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'Error while creating new ws -> sms mapping');
                res.redirect(settings.redirectTo);
            });
    }
};


// EMAIL -> SMS
function getUserEmailSmsSettings(req, res, next) {
    console.log("Getting all email in settings " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select es.source_email_address, 
        es.created_at, 
        es.modified_at,
        es.description,
        es.email_identification, 
        es.active_status,
        es.id as setting_id
        from email_forward_in_settings es
        left join registered_user ruser on es.user_id = ruser.id 
        where ruser.email = '` + req.user.local.email + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}

function getServerEmail(req, res) {
    console.log("getting server email " + req.user);

    smsGatewayBI.getServerEmail(req.user.local.email, function (error, result, body) {
        if (!result){
            res.status(200).json({
                status: 'error'
            });
        }
        else if (result.statusCode == 200) {
            res.status(200).json({
                status: 'success',
                data: body
            });
        } else {
            res.status(200).json({
                status: 'error'
            });
        }
    });
}


function updateEmailSmsSettings(req, res, settings) {

    console.log("updating ws -> sms mapping for user" + req.user.local.email);

    console.log(req.body);

    if (req.body.setting_id) {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`update email_forward_in_settings set 
        modified_at = now(), 
        source_email_address = '` + req.body.source_email_address + `', 
        description = '` + req.body.description + `', 
        active_status = '` + req.body.active_status + `'
            where id = '` + req.body.setting_id + `'`)
            .then(function (data) {
                req.flash('successMessage', 'Data was successfully updated');
                res.redirect(settings.redirectTo);
            })
            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'error');
                res.redirect(settings.redirectTo);
            });
    } else {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`insert into email_forward_in_settings  (modified_at, created_at, source_email_address, description, email_identification, active_status, user_id) values (now(), now(), '` + req.body.source_email_address + `','` + req.body.description +
            `','` + uuidV4() +
            `','` + req.body.active_status +
            `',(select id from registered_user where email = '` + req.user.local.email + `'))`
        )
            .then(function (data) {
                req.flash('successMessage', 'New email -> sms mapping was successfully created');
                res.redirect(settings.redirectTo);
            })
            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'Error while creating new email -> sms mapping');
                res.redirect(settings.redirectTo);
            });
    }
};

// SMS -> EMAIL


function getUserSmsEmailSettings(req, res, next) {
    console.log("Getting all email out settings " + req.user);

    //noinspection JSAnnotator
    db_connector.db_instance.any(`select 
es.destination_email_address, 
es.subject, 
es.body, 
es.created_at, 
es.modified_at, 
es.description,
es.id as setting_id, 
es.active_status,
cscd.device_name, 
cscd.id as device_id
from email_forward_out_settings as es 
left join client_secure_communication_data cscd on es.client_id = cscd.id
left join registered_user ruser on cscd.user_id = ruser.id  where ruser.email = '` + req.user.local.email + "'")
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Query successfully processed'
                });
        })
        .catch(function (err) {
            console.error(err);
            return next(err);
        });
}


function updateSmsEmailSetting(req, res, settings) {

    console.log("updating sms -> email mapping for user" + req.user.local.email);

    console.log(req.body);

    if (req.body.setting_id) {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`update email_forward_out_settings set 
        modified_at = now(), 
        destination_email_address = '` + req.body.destination_email_address + `', 
        description = '` + req.body.description + `', 
        subject = '` + req.body.subject + `', 
        body = '` + req.body.body + `', 
        client_id = '` + req.body.device_id + `',
        active_status = '` + req.body.active_status + `'
            where id = '` + req.body.setting_id + `'`)
            .then(function (data) {
                req.flash('successMessage', 'Data was successfully updated');
                res.redirect(settings.redirectTo);
            })
            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'error');
                res.redirect(settings.redirectTo);
            });
    } else {
        //noinspection JSAnnotator
        db_connector.db_instance.any(`insert into email_forward_out_settings  (
        modified_at, 
        created_at, 
        destination_email_address, 
        description, 
        subject, 
        body, 
        active_status, 
        client_id) values 
        (now(), 
        now(), 
            '` + req.body.destination_email_address +
            `','` + req.body.description +
            `','` + req.body.subject +
            `','` + req.body.body +
            `','` + req.body.active_status +
            `','` + req.body.device_id +
            `')`
        )
            .then(function (data) {
                req.flash('successMessage', 'New ws -> sms mapping was successfully created');
                res.redirect(settings.redirectTo);
            })
            .catch(function (err) {
                console.error(err);
                req.flash('errorMessage', 'Error while creating new ws -> sms mapping');
                res.redirect(settings.redirectTo);
            });
    }
};


module.exports = {
    getUserWsSmsSettings: getUserWsSmsSettings,
    getServerWsdl: getServerWsdl,
    updateWsSmsSetting: updateWsSmsSetting,

    getUserSmsWsSettings: getUserSmsWsSettings,
    updateSmsWsSetting: updateSmsWsSetting,

    getUserSmsEmailSettings: getUserSmsEmailSettings,
    updateSmsEmailSetting: updateSmsEmailSetting,

    getUserEmailSmsSettings: getUserEmailSmsSettings,
    getServerEmail: getServerEmail,
    updateEmailSmsSettings: updateEmailSmsSettings

};