/**
 * Created by martinhudec on 24/04/2017.
 */
var dbConnector = require('../config/pgDatabase.js');
var request = require('request');

//var url =  'http://smsgatewaydp.ddns.net:4567/api/';
//var url = 'http://192.168.21.3:4567/api/';
var url = 'http://localhost:4567/api/';

var request_options_post = function (operation, data) {
    return {
        url: url + operation,
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        json: true,
        body: data
    }
};


var request_options_get = function (operation, data) {
    //url: 'http://smsgatewaydp.ddns.net:4567/api/createUser',
    return {
        //url: 'http://smsgatewaydp.ddns.net:4567/api/' + operation,
        url: url + operation,
        method: 'GET',
        headers: {
            "Content-Type": "application/json"
        },
        json: true,
        qs: data
    }
};


function createNewUser(email, callback) {
    console.log("Creating new user with email " + email);

    var req = request_options_post('createUser', {'userEmail': email});
    request(req, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, undefined, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, response.statusCode);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, undefined, response.statusCode);
            }
        }
    );
}

function registerNewDevice(email, device_name, description, mobile_number, callback) {
    console.log("Creating new user with email " + email);
    var req = request_options_post('registerNewDevice', {
        'deviceName': device_name,
        'description': description,
        'mobileNumberId': mobile_number,
        'userEmail': email
    });

    request(req, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, undefined, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, response.statusCode);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, undefined, response.statusCode);
            }
        }
    );
}

function generateCustomAlphabet(email, data_set, deviceId, callback) {
    console.log("generating new custom alphabet for user with email " + email);
    var request_ops = request_options_post('generateCustomAlphabet', {
        'dataSet': data_set,
        'deviceId': deviceId,
        'userEmail': email
    });

    request(request_ops, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, undefined, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, response.statusCode);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, undefined, response.statusCode);
            }
        }
    );

}

function sendTechnicalPingMessage(email, deviceId, callback) {
    console.log("sending test message for user with email " + email);
    var request_ops = request_options_post('sendTechnicalPingMessage', {
        'deviceId': deviceId,
        'userEmail': email
    });

    request(request_ops, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, undefined, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, response.statusCode);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, undefined, response.statusCode);
            }
        }
    );

}

function sendKeyExchangeRequest(email, deviceId, callback) {
    console.log("sending sendKeyExchangeRequest for user with email " + email);
    var request_ops = request_options_post('sendKeyExchangeRequest', {
        'deviceId': deviceId,
        'userEmail': email
    });

    request(request_ops, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, undefined, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, response.statusCode);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, undefined, response.statusCode);
            }
        }
    );

}

function getServerWsdl(email, callback) {
    console.log("sending request for getServerWsdl " + email);
    var request_ops = request_options_get('getServerApiWsdl', undefined);



    request(request_ops, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, response, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, body);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, response, response.statusCode);
            }
        }
    );

}

function getServerEmail(email, callback) {
    console.log("sending request for getServerEmail " + email);
    var request_ops = request_options_get('getServerApiEmail', undefined);



    request(request_ops, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, response, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, body);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, response, response.statusCode);
            }
        }
    );

}


function getMessageDetail(messageId, callback) {

    var request_ops = request_options_get('getMessageDetail', {
        'messageId': messageId
    });


    console.log(request_ops);
    request(request_ops, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, response, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, body);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, response, response.statusCode);
            }
        }
    );

}

function getSentMessageDetail(messageId, callback) {

    var request_ops = request_options_get('getSentMessageDetail', {
        'messageId': messageId
    });


    console.log(request_ops);
    request(request_ops, function (error, response, body) {
            console.log("body: " + JSON.stringify(body));
            if (response === undefined || response == null) {
                console.log("returning callback");
                return callback(undefined, undefined, undefined);
            }
            if (error) {
                console.error(error, response.statusCode);
                return callback(error, response, response.statusCode);
            } else if (response.statusCode === 200) {
                return callback(undefined, response, body);
            } else {
                console.error("invalid response code from api " + response.statusCode);
                return callback(error, response, response.statusCode);
            }
        }
    );

}




module.exports = {
    createNewUser: createNewUser,
    registerNewDevice: registerNewDevice,
    generateCustomAlphabet: generateCustomAlphabet,
    sendTechnicalPingMessage: sendTechnicalPingMessage,
    sendKeyExchangeRequest: sendKeyExchangeRequest,
    getServerWsdl: getServerWsdl,
    getServerEmail: getServerEmail,
    getMessageDetail: getMessageDetail,
    getSentMessageDetail: getSentMessageDetail
};